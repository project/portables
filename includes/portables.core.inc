<?php
/**
 * @file
 * Portables core classes and Rules implementations.
 */

/**
 * A portable is a coherent container for data sources and processing tasks.
 */
class Portable extends RulesPlugin {
  protected $itemName = 'portable';

  /**
   * Rebuilds plugin cache.
   */
  public function rebuildCache(&$itemInfo, &$cache) {
    parent::rebuildCache($itemInfo, $cache);

    // Track portable references.
    $cache['portables_info'] = array('portable' => array(), 'source' => array(), 'task' => array());
    $sourcePlugins = portables_source_plugins();
    $taskPlugins = portables_task_plugins();
    $entities = rules_config_load_multiple(FALSE, array('plugin' => array_merge(array('portable'), $sourcePlugins, $taskPlugins)));
    foreach ($entities as $name => $entity) {
      /** @var $entity RulesPlugin */
      $plugin = $entity->plugin();
      $info = $entity->info();
      // Track portables.
      if ($plugin == 'portable') {
        $cache['portables_info']['portable'] += array($name => array());
        $cache['portables_info']['portable'][$name] += array('sources' => array(), 'tasks' => array());
      }
      // Track sources.
      elseif (in_array($plugin, $sourcePlugins)) {
        $portableName = $info['portable'];
        $cache['portables_info']['source'][$name] = array(
          'portable' => $portableName,
        );
        $cache['portables_info']['portable'][$portableName]['sources'][] = $name;
      }
      // Track tasks.
      elseif (in_array($plugin, $taskPlugins)) {
        $portableName = $info['portable'];
        $cache['portables_info']['task'][$name] = array(
          'portable' => $portableName,
        );
        $cache['portables_info']['portable'][$portableName]['tasks'][] = $name;
      }
    }
  }

  public function executeByArgs($args = array()) {
    // TODO
  }

  public function evaluate(RulesState $state) {
    // TODO
  }
}

/**
 * Interface for a unit element belonging to a portable, usually as an entity.
 */
interface PortablesPortableUnitInterface {
  /**
   * Updates the portable containing this unit.
   *
   * @param string $portable
   *   Name of a portable.
   */
  public function setPortable($portable);

  /**
   * Returns the name of the portable.
   *
   * @return string
   */
  public function getPortable();
}

/**
 * Default portable unit element class.
 */
abstract class PortablesPortableUnit extends RulesPlugin implements PortablesPortableUnitInterface {
  /**
   * Creates a source.
   *
   * @param string $portable
   *   Name of a portable containing this unit.
   */
  public function __construct($portable = NULL) {
    $this->info += array('portable' => NULL);
    if (isset($portable)) {
      $this->setPortable($portable);
    }

    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public function setPortable($portable) {
    $this->info['portable'] = $portable;
    $this->resetInternalCache();
  }

  /**
   * {@inheritdoc}
   */
  public function getPortable() {
    return $this->info['portable'];
  }

  /**
   * Persists plugin info.
   */
  public function __sleep() {
    return parent::__sleep() + array('info' => 'info');
  }

  /**
   * {@inheritdoc}
   */
  public function save($name = NULL, $module = 'rules') {
    $return = parent::save($name, $module);

    // Flush cache on creation of unit.
    if ($return == SAVED_NEW) {
      rules_clear_cache();
    }

    return $return;
  }
}

/**
 * Base class for a data source.
 */
abstract class PortablesSource extends PortablesPortableUnit {
  public function executeByArgs($args = array()) {
    // TODO
  }

  public function evaluate(RulesState $state) {
    // TODO
  }
}

/**
 * Processing task.
 */
abstract class PortablesTask extends PortablesPortableUnit {
  /**
   * Creates a task.
   *
   * @param string $portable
   *   Name of a portable.
   * @param string $selector
   *   Rules data selector on which to trigger the task.
   */
  public function __construct($portable = NULL, $selector = NULL) {
    $this->info += array(
      'selector' => NULL,
    );
    if (isset($selector)) {
      $this->info['selector'] = $selector;
    }

    parent::__construct($portable);
  }

  public function executeByArgs($args = array()) {
    // TODO
  }

  public function evaluate(RulesState $state) {
    // TODO
  }
}
