<?php
/**
 * @file
 * Portables core task implementation.
 */

/**
 * Basic rule task.
 */
class PortablesRuleTask extends PortablesTask {
  protected $itemName = 'portables rule task';
}
