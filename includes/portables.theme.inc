<?php
/**
 * @file
 * Theming implementations.
 */

/**
 * Defines theme hooks.
 */
function _portables_theme() {
  $base = array(
    'file' => 'portables.theme.inc',
    'path' => drupal_get_path('module', 'portables') . '/includes',
  );
  return array(
    'portables_context_trail' => array(
      'variables' => array('trail' => array()),
    ) + $base,
    'portables_context_trail_separator' => array(
      'variables' => array(),
    ) + $base,
    'portables_multistep' => array(
      'render element' => 'element',
    ) + $base,
    'portables_multistep_legend' => array(
      'variables' => array('legend' => array(), 'legend_title' => ''),
    ) + $base,
    'portables_multistep_tabs' => array(
      'variables' => array('tabs' => array(), 'active_tab' => ''),
    ) + $base,
  );
}

/**
 * Preprocesses a plugin context trail.
 */
function template_preprocess_portables_context_trail(&$variables) {
  $cache = rules_get_cache();

  // Formats links from plugins.
  $items = array();
  foreach ($variables['trail'] as $element) {
    if (is_array($element)) {
      $type = isset($element['type']) ? $element['type'] : '';
      $element = $element['element'];
    }
    /** @var $element RulesPlugin */
    if ($element) {
      $plugin = $element->plugin();
      $plugin = isset($cache['plugin_info'][$plugin]['label']) ? $cache['plugin_info'][$plugin]['label'] : drupal_ucfirst($plugin);

      // Build trail item.
      $item = array(
        '#prefix' => '<span class="trail-item">',
        '#suffix' => '</span>',
      );
      $item['plugin'] = array(
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => !empty($type) ? $type : $plugin,
        '#attributes' => array(
          'class' => array('element-plugin'),
        ),
      );
      $item['label'] = array(
        '#type' => 'link',
        '#title' => $element->label(),
        '#href' => RulesPluginUI::path($element->name),
      );
      $items[] = $item;
    }
  }
  $variables['items'] = $items;
}

/**
 * Renders a context trail.
 */
function theme_portables_context_trail($variables) {
  // Render trail.
  $trail = array(
    '#theme_wrappers' => array('container'),
    '#attributes' => array(
      'class' => array('portables-context-trail', 'clearfix'),
    ),
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'portables') . '/ui/portables.ui.css'),
    ),
  );
  $trail['title'] = array(
    '#type' => 'html_tag',
    '#tag' => 'span',
    '#value' => t('Editing'),
    '#attributes' => array(
      'class' => array('title'),
    ),
  );
  foreach (array_values($variables['items']) as $i => $item) {
    if ($i > 0) {
      $trail['items']["{$i}_separator"] = array(
        '#theme' => 'portables_context_trail_separator',
      );
    }
    $trail['items']["$i"] = $item;
  }

  return drupal_render($trail);
}

/**
 * Renders a separator between plugin context trail items.
 */
function theme_portables_context_trail_separator($variables) {
  return '<span class="trail-separator">&raquo;</span>';
}

/**
 * Renders a multi-step UI element.
 */
function theme_portables_multistep($variables) {
  $element = $variables['element'];
  $children = '';
  $attributes = array();
  if (isset($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  $attributes['class'] = 'form-portables-multistep clearfix';
  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] .= ' ' . implode(' ', $element['#attributes']['class']);
  }
  if (!empty($element['#legend'])) {
    $children .= theme('portables_multistep_legend', array('legend' => $element['#legend'], 'legend_title' => $element['#legend_title']));
  }
  $children .= $element['#tabs_rendered'];
  $children .= '<div class="portables-multistep-content"><div class="inside">' . $element['#children'] . '</div></div>';
  return '<div' . drupal_attributes($attributes) . '>' . $children . '</div>';
}

/**
 * Prepares a multi-step UI legend.
 */
function template_preprocess_portables_multistep_legend(&$variables) {
  $items = array();
  foreach ($variables['legend'] as $name => $description) {
    $items[$name] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('legend-item'),
      ),
    );
    $items[$name]['key'] = array(
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => array(
        'class' => array('legend-key', drupal_html_class($name)),
      ),
      '#value' => '',
    );
    $items[$name]['description'] = array(
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => $description,
      '#attributes' => array(
        'class' => array('legend-description'),
      ),
    );
  }
  $variables['items'] = $items;
}

/**
 * Renders a legend for the multi-step UI.
 */
function theme_portables_multistep_legend($variables) {
  $legend = array(
    '#theme_wrappers' => array('container'),
    '#attributes' => array('class' => array('portables-multistep-legend')),
  );
  if (!empty($variables['legend_title'])) {
    $legend['heading'] = array(
      '#markup' => $variables['legend_title'],
      '#theme_wrappers' => array('container'),
      '#attributes' => array('class' => array('legend-heading')),
    );
  }
  $legend['items'] = $variables['items'];
  return drupal_render($legend);
}

/**
 * Renders the multi-step UI tabset.
 */
function theme_portables_multistep_tabs($variables) {
  // Build tabs.
  $links = array();
  $active_tab = $variables['active_tab'];
  foreach ($variables['tabs'] as $name => $link) {
    // Mark active tab.
    if ($name == $active_tab) {
      $link['attributes']['class'][] = 'active-tab';
    }
    // Add link.
    $links[drupal_html_class('tab-' . $name)] = $link;
  }

  // Render tab links.
  $links_variables = array('links' => $links);
  $links_variables['attributes']['class'][] = 'portables-multistep-tabs';
  $output = theme('links__portables_multistep_tabs', $links_variables);

  return $output;
}
