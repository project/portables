<?php
/**
 * @file
 * Administrative form callbacks.
 */

/**
 * Returns an overview table of import configurations.
 */
function portables_ui_configuration_overview($form, &$form_state, $base_path) {
  PortablesPluginUI::formDefaults($form, $form_state, $base_path);

  // Obtain filter argument.
  $collapsed = TRUE;
  $conditions = array('plugin' => 'portable');
  if (empty($_GET['tag'])) {
    $tag = 0;
  }
  else {
    $tag = $_GET['tag'];
    $conditions['tags'] = array($tag);
    $collapsed = FALSE;
  }

  // Add tag filter fieldset.
  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter'),
    '#collapsible' => TRUE,
    '#collapsed' => $collapsed,
  );
  $form['filter']['#id'] = 'rules-filter-form';
  $form['filter']['tag'] = array(
    '#type' => 'select',
    '#title' => t('Filter by tag'),
    '#options' => array(0 => '<All>') + RulesPluginUI::getTags(),
    '#default_value' => $tag,
  );
  $form['filter']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#name' => '',
  );

  $form['table'] = portables_ui()->overviewTable($conditions, array(
    'show plugin' => FALSE,
    'hide status op' => TRUE,
    'hide clone op' => TRUE,
  ));
  $form['table']['#empty'] = t('There are no portables.');

  $form['#submit'][] = 'rules_form_submit_rebuild';
  $form['#method'] = 'get';
  return $form;
}

/**
 * Returns the settings form.
 */
function portables_ui_settings($form, &$form_state) {
  // TODO

  return system_settings_form($form);
}
