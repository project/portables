<?php
/**
 * @file
 * Documentation for extending Portables workflow plugins.
 */

/**
 * @defgroup portables Portables module integration.
 *
 * Rules-based integration with the Portables framework.
 */

/**
 * @defgroup portables_hooks Portables hooks
 * @{
 */

/**
 * Provide portable source file types.
 */
function hook_portables_file_info() {
  // TODO
  return array();
}

/**
 * @}
 */
