<?php
/**
 * @file
 * Main Portables module architecture.
 */

/**
 * Loads a portable.
 */
function portable_load($name) {
  $entities = rules_config_load_multiple(array($name), array('plugin' => 'portable'));
  return $entities ? reset($entities) : NULL;
}

/**
 * Implements hook_permission().
 */
function portables_permission() {
  return array(
    'administer portables' => array(
      'title' => t('Administer Portables'),
      'description' => t('Administer configuration of source and task assemblies.'),
    ),
  );
}

/**
 * Returns a page title for the UI controller.
 */
function portables_get_title($text, $element) {
  // Returns the defined plugin label or plugin name with case unchanged.
  if ($element instanceof RulesPlugin) {
    $cache = rules_get_cache();
    $plugin = $element->plugin();
    $plugin = isset($cache['plugin_info'][$plugin]['label']) ? $cache['plugin_info'][$plugin]['label'] : $plugin;
    return t($text, array('!label' => $element->label(), '!plugin' => $plugin));
  }
  // Fall back to default Rules title.
  return rules_get_title($text, $element);
}

/**
 * Returns a single instance of UI controller.
 */
function portables_ui() {
  $instance = &drupal_static(__FUNCTION__);
  if (!isset($instance)) {
    $instance = new PortablesUIController();
  }
  return $instance;
}

/**
 * Implements hook_theme().
 */
function portables_theme() {
  module_load_include('inc', 'portables', 'includes/portables.theme');
  return _portables_theme();
}

/**
 * Returns a list of source plugin names.
 */
function portables_source_plugins() {
  return _portables_list_plugins_by_flag('portables source');
}

/**
 * Returns list of of source plugin names.
 */
function portables_task_plugins() {
  return _portables_list_plugins_by_flag('portables task');
}

/**
 * Lists plugin names by flag.
 */
function _portables_list_plugins_by_flag($flag) {
  $cache = rules_get_cache();
  $plugins = array_keys(rules_filter_array($cache['plugin_info'], $flag, TRUE));
  return $plugins;
}

/**
 * Implements hook_element_info().
 */
function portables_element_info() {
  return array(
    'portables_plugin_chooser' => array(
      '#input' => TRUE,
      '#plugins' => array(),
      '#process' => array('portables_plugin_chooser_process'),
      '#theme_wrappers' => array('form_element'),
    ),
    'portables_multistep' => array(
      '#legend' => array(),
      '#legend_title' => t('Legend'),
      '#tabs' => array(),
      '#tree' => TRUE,
      '#active_tab' => '',
      '#theme_wrappers' => array('portables_multistep'),
      '#process' => array('portables_multistep_process'),
      '#pre_render' => array('portables_multistep_pre_render'),
    ),
    'portables_file' => array(
      '#input' => TRUE,
      '#file' => NULL,
      '#file_type' => NULL,
      '#process' => array('portables_file_process'),
      '#theme_wrappers' => array('form_element'),
    ),
  );
}

/**
 * Processes a plugin chooser form element.
 */
function portables_plugin_chooser_process($element) {
  $cache = rules_get_cache();

  // Process element as radios.
  $options = array();
  foreach ($element['#plugins'] as $key) {
    $options[$key] = isset($cache['plugin_info'][$key]['label']) ?
      $cache['plugin_info'][$key]['label'] :
      drupal_ucfirst($key);
  }
  $element['#options'] = $options;
  $element = form_process_radios($element);

  // Process plugin radios.
  foreach ($element['#plugins'] as $key) {
    // Add descriptions.
    if (!empty($cache['plugin_info'][$key]['description'])) {
      $element[$key]['#description'] = $cache['plugin_info'][$key]['description'];
    }
  }

  return $element;
}

/**
 * Processes a multi-step UI element.
 */
function portables_multistep_process($element) {
  // Attach style sheet.
  $element['#attached']['css'][] = drupal_get_path('module', 'portables') . '/ui/portables.ui.css';
  return $element;
}

/**
 * Processes a multi-step UI prior to rendering.
 */
function portables_multistep_pre_render($element) {
  // Render multi-step UI tabs.
  $element['#tabs_rendered'] = theme('portables_multistep_tabs', array(
    'tabs' => $element['#tabs'],
    'active_tab' => $element['#active_tab'],
  ));
  return $element;
}
