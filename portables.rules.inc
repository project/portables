<?php
/**
 * @file
 * Rules integration.
 */

/**
 * Implements hook_rules_plugin_info().
 */
function portables_rules_plugin_info() {
  return array(
    'portable' => array(
      'class' => 'Portable',
      'embeddable' => FALSE,
      'extenders' => array(
        'RulesPluginUIInterface' => array(
          'class' => 'PortablesPortableUI',
        ),
      ),
    ),
    'portables xpath source' => array(
      'label' => t('XPath source'),
      'description' => t('Structured data extracted from an XML document using XPath queries.'),
      'class' => 'PortablesXpathSource',
      'embeddable' => FALSE,
      'portables source' => TRUE,
      'extenders' => array(
        'RulesPluginUIInterface' => array(
          'class' => 'PortablesXpathSourceUI',
        ),
      ),
    ),
    'portables rule task' => array(
      'label' => t('Rule'),
      'description' => t('The rule actions '),
      'class' => 'PortablesRuleTask',
      'embeddable' => FALSE,
      'extenders' => array(
        'RulesPluginUIInterface' => array(
          'class' => 'PortablesTaskUI',
        ),
      ),
    ),
  );
}
