<?php
/**
 * @file
 * UI controller implementation.
 */

/**
 * UI controller for Portables plugins.
 */
class PortablesUIController extends RulesUIController {
  /**
   * {@inheritdoc}
   */
  public function config_menu($base_path) {
    $items = parent::config_menu($base_path);
    $base_count = count(explode('/', $base_path));

    // Adjust page titles.
    $adjust_paths[] = $base_path . '/manage/%rules_config';
    $adjust_paths[] = $base_path . '/manage/%rules_config/edit/%rules_element';
    $adjust_paths[] = $base_path . '/manage/%rules_config/delete/%rules_element';
    $adjust_paths[] = $base_path . '/manage/%rules_config/clone';
    $adjust_paths[] = $base_path . '/manage/%rules_config/export';
    $adjust_paths[] = $base_path . '/manage/%rules_config/execute';
    $adjust_paths[] = $base_path . '/manage/%rules_config/schedule';
    foreach ($adjust_paths as $path) {
      if (isset($items[$path]['title callback']) && $items[$path]['title callback'] == 'rules_get_title') {
        $items[$path]['title callback'] = 'portables_get_title';
      }
    }

    $items[$base_path . '/add-source/%portable'] = array(
      'title callback' => 'rules_get_title',
      'title arguments' => array('Add new source for "!label"', $base_count + 1),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('portables_ui_add_source', $base_count + 1, $base_path),
      'access arguments' => array('administer portables'),
      'file' => 'ui/ui.forms.inc',
      'file path' => drupal_get_path('module', 'portables'),
    );
    $items[$base_path . '/add-task/%portable'] = array(
      'title callback' => 'rules_get_title',
      'title arguments' => array('Add new task for "!label"', $base_count + 1),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('portables_ui_add_task', $base_count + 1, $base_path),
      'access arguments' => array('administer portables'),
      'file' => 'ui/ui.forms.inc',
      'file path' => drupal_get_path('module', 'portables'),
    );

    return $items;
  }

  /**
   * Displays an overview table.
   *
   * @param $conditions
   *   An array of conditions as needed by rules_config_load_multiple().
   * @param $options
   *   An array with options supported by RulesUIController::overviewTable(),
   *   plus the following:
   *   - 'caption': A renderable array or string for table caption.
   *   - 'add links': A renderable array for a row of links to add plugins.
   *   - 'hide clone op': If set to TRUE, the link to clone a configuration is
   *     hidden from a table row.
   *
   * @return array
   */
  public function overviewTable($conditions = array(), $options = array()) {
    $options += array(
      'caption' => NULL,
      'add links' => NULL,
      'hide clone op' => FALSE,
    );

    // Build table.
    $table = parent::overviewTable($conditions, $options);
    $colspan = 0;
    foreach ($table['#header'] as $cell) {
      $colspan += is_array($cell) && isset($cell['colspan']) ? $cell['colspan'] : 1;
    }

    // Add empty text.
    if (empty($table['#rows'])) {
      $table['#rows'][]['data'][] = array('data' => t('None'), 'colspan' => $colspan);
    }

    // Add links row.
    if (!empty($options['add links'])) {
      $row = array();
      $row[] = array('data' => $options['add links'], 'colspan' => $colspan);
      $table['#rows'][] = array('data' => $row, 'class' => array('rules-elements-add'));
    }

    // Add table caption.
    if (!empty($options['caption'])) {
      $table['#caption'] = $options['caption'];
    }

    return $table;
  }

  /**
   * Displays a row in an overview table.
   *
   * This implementation optionally hides the clone link.
   */
  protected function overviewTableRow($conditions, $name, $config, $options) {
    $row[] = array('data' => $config->buildContent());
    if ($options['show plugin']) {
      $plugin = $config->plugin();
      $row[] = isset($this->cache['plugin_info'][$plugin]['label']) ? $this->cache['plugin_info'][$plugin]['label'] : $plugin;
    }
    $row[] = array('data' => array(
      '#theme' => 'entity_status',
      '#status' => $config->status,
    ));
    // Add operations depending on the options and the exportable status.
    if (!$config->hasStatus(ENTITY_FIXED)) {
      $row[] =  l(t('edit'), RulesPluginUI::path($name));
    }
    else {
      $row[] = '';
    }

    if (!$options['hide status op']) {
      // Add either an enable or disable link.
      $text = $config->active ? t('disable') : t('enable');
      $link_path = RulesPluginUI::path($name, $config->active ? 'disable' : 'enable');
      $row[] = $config->hasStatus(ENTITY_FIXED) ? '' : l($text, $link_path, array('query' => drupal_get_destination()));
    }
    if (!$options['hide clone op']) {
      $row[] = l(t('clone'), RulesPluginUI::path($name, 'clone'));
    }

    if ($options['show execution op']) {
      $row[] = ($config instanceof RulesTriggerableInterface) ? '' : l(t('execute'), RulesPluginUI::path($name, 'execute'), array('query' => drupal_get_destination()));
    }

    // Add delete link.
    if (!$config->hasStatus(ENTITY_IN_CODE) && !$config->hasStatus(ENTITY_FIXED)) {
      $row[] = l(t('delete'), RulesPluginUI::path($name, 'delete'), array('query' => drupal_get_destination()));
    }
    elseif ($config->hasStatus(ENTITY_OVERRIDDEN) && !$config->hasStatus(ENTITY_FIXED)) {
      $row[] = l(t('revert'), RulesPluginUI::path($name, 'revert'), array('query' => drupal_get_destination()));
    }
    else {
      $row[] = '';
    }

    // Add export link.
    $row[] = l(t('export'), RulesPluginUI::path($name, 'export'));

    return $row;
  }
}
