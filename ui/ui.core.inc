<?php
/**
 * @file
 * Portables core UI classes.
 */

/**
 * Base plugin UI to adapt base Rules UI for use in Portables workflow.
 */
abstract class PortablesPluginUI extends RulesPluginUI {
  /**
   * {@inheritdoc}
   */
  public function form(&$form, &$form_state, $options = array()) {
    parent::form($form, $form_state, $options);

    // Display portable context trail.
    if (empty($options['init']) && $context = $this->getPluginContext()) {
      $form['context'] = array(
        '#markup' => theme('portables_context_trail', array('trail' => $context)),
      );
    }
  }

  /**
   * Returns the plugin context trail.
   */
  protected function getPluginContext() {
    $element = $this->element;
    $context = array();
    // Add the containing portable for a unit.
    if ($element instanceof PortablesPortableUnit) {
      $context[] = portable_load($element->getPortable());
    }
    // Add the element.
    $context[] = $this->getThisPluginContext();
    return count($context) > 1 ? $context : NULL;
  }

  /**
   * Returns the context trail item for this element.
   */
  protected function getThisPluginContext() {
    $element = $this->element;
    $info = $element->pluginInfo();
    $type = NULL;
    if ($info['portables source'])  {
      $type = t('Source');
    }
    elseif ($info['portables task']) {
      $type = t('Task');
    }
    return empty($type) ? $element : array(
      'type' => $type,
      'element' => $element,
    );
  }

  /**
   * Tweaks the default plugin settings form.
   */
  public function settingsForm(&$form, &$form_state) {
    parent::settingsForm($form, $form_state);

    // Strip 'portables_' prefix.
    if (!empty($this->element->module) && !empty($this->element->name) && $this->element->module == 'portables' && strpos($this->element->name, 'portables_') === 0) {
      // Apply alternate machine name.
      $form['settings']['name']['#default_value'] = substr($this->element->name, strlen($this->element->module) + 1);
    }

    // Hide variables.
    $form['settings']['vars']['#access'] = FALSE;
  }

  /**
   * Builds the display of the plugin in overview.
   */
  public function buildContent() {
    $content = parent::buildContent();

    if ($this->element->isRoot()) {
      // Add machine name label.
      if (!$this->element->active) {
        $content['label']['#suffix'] = ' <em>' . t('(@disabled)', array('@disabled' => t('disabled'))) . '</em>' . $content['label']['#suffix'];
      }
      $content['description']['settings'] = array(
        '#theme' => 'rules_content_group',
        '#weight' => -4,
        'machine_name' => array(
          '#markup' => t('Machine name') . ': ' . $this->element->name,
        ),
      );
      // Add tags.
      if (!empty($this->element->tags)) {
        $content['description']['tags'] = array(
          '#theme' => 'rules_content_group',
          '#caption' => t('Tags'),
          'tags' => array(
            '#markup' => implode(', ', $this->element->tags),
          ),
        );
      }
    }

    return $content;
  }

  /**
   * {@inheritdoc}
   *
   * The usual value for $action is 'manage'.
   */
  public static function path($action, $name, $op = NULL, RulesPlugin $element = NULL, $parameter = FALSE) {
    // Determine base path.
    if (isset(RulesPluginUI::$basePath)) {
      $basePath = RulesPluginUI::$basePath;
    }
    else {
      $basePath = 'admin/config/workflow/portables/config';
    }

    // Construct path.
    $elementId = isset($element) ? $element->elementId() : FALSE;
    return implode('/', array_filter(array($basePath, $action, $name, $op, $elementId, $parameter)));
  }

  /**
   * {@inheritdoc}
   */
  public static function formDefaults(&$form, &$form_state, $base_path = NULL) {
    if (isset($base_path)) {
      RulesPluginUI::$basePath = $base_path;
    }
    RulesPluginUI::formDefaults($form, $form_state);
    form_load_include($form_state, 'inc', 'portables', 'ui/ui.forms');
  }
}

/**
 * UI for portables.
 */
class PortablesPortableUI extends PortablesPluginUI {
  /**
   * Adds tags assigned for this portable to display.
   */
  public function buildContent() {
    $content = parent::buildContent();

    if (!empty($this->element->tags)) {
      $content['description']['tags'] = array(
        '#theme' => 'rules_content_group',
        '#caption' => t('Tags'),
        'tags' => array(
          '#markup' => implode(', ', $this->element->tags),
        ),
      );
    }

    return $content;
  }

  /**
   * Returns a basic settings form.
   */
  public function form(&$form, &$form_state, $options = array()) {
    parent::form($form, $form_state, $options);
    $cache = rules_get_cache();

    // Display help text.
    $form['help'] = array(
      '#weight' => -5,
      '#prefix' => '<p>',
      '#markup' => t('A portable groups data sources and processing tasks. The portable simplifies task configuration by determining the variables provided by the collection of contained sources.'),
      '#suffix' => '</p>',
    );

    // List sources in table.
    $sources = $cache['portables_info']['portable'][$this->element->name]['sources'];
    $form['sources'] = portables_ui()->overviewTable(array('name' => $sources ? $sources : NULL), array(
      'add links' => array(
        '#theme' => 'links__rules',
        '#attributes' => array(
          'class' => array('rules-operations-add', 'action-links'),
        ),
        '#links' => array(
          'add_source' => array(
            'title' => t('Add !name', array('!name' => t('source', array(), array('context' => 'portables')))),
            'href' => PortablesPluginUI::path('add-source', $this->element->name),
          ),
        ),
      ),
    ));
    $form['sources']['#attributes']['class'][] = 'rules-elements-table';
    $form['sources']['#caption'] = t('Sources');

    // List tasks in table.
    $tasks = $cache['portables_info']['portable'][$this->element->name]['tasks'];
    $form['tasks'] = portables_ui()->overviewTable(array('name' => $tasks ? $tasks : NULL), array(
      'add links' => array(
        '#theme' => 'links__rules',
        '#attributes' => array(
          'class' => array('rules-operations-add', 'action-links'),
        ),
        '#links' => array(
          'add_task' => array(
            'title' => t('Add !name', array('!name' => t('task', array(), array('context' => 'portables')))),
            'href' => PortablesPluginUI::path('add-task', $this->element->name),
          ),
        ),
      ),
    ));
    $form['tasks']['#attributes']['class'][] = 'rules-elements-table';
    $form['tasks']['#caption'] = t('Tasks');
  }
}

/**
 * Basic, flexible multi-step UI for portable units.
 */
abstract class PortablesMultiStepPluginUI extends PortablesPluginUI {
  /**
   * Status for an incomplete step.
   */
  const INCOMPLETE = 'incomplete';

  /**
   * Status for a completed step.
   */
  const COMPLETE = 'complete';

  /**
   * Step UI instance pool.
   * @var PortablesStepUI[]
   */
  protected $steps = array();

  /**
   * Displays the multi-step frame and active step configuration form.
   */
  public function form(&$form, &$form_state, $options = array()) {
    parent::form($form, $form_state, $options);

    if (empty($options['init'])) {
      // Display step UI tabs.
      $allSteps = $this->getStepUIAll();
      $tabs = array();
      $activeStep = NULL;
      foreach ($this->getStepInfo() as $name => $info) {
        $tabs[$name] = array(
          'title' => check_plain($info['label']),
          'html' => TRUE,
          'href' => RulesPluginUI::path($this->element->name),
          'query' => array('step' => $name),
        );
        if (isset($allSteps[$name])) {
          $statusClass = drupal_html_class('step-' . $allSteps[$name]->getStatus());
          $tabs[$name]['title'] = '<span class="' . $statusClass . '">' . $tabs[$name]['title'] . '</span>';
        }
        // Detect active step.
        if ($_GET['step'] == $name) {
          $activeStep = $name;
        }
      }

      // Default active step.
      if (!isset($activeStep)) {
        $activeStep = $this->getDefaultStep();
        if (!isset($activeStep)) {
          // Default to first step.
          $activeStep = key($tabs);
        }
      }

      // Request each step to prepare form.
      foreach ($allSteps as $ui) {
        if ($ui) {
          $ui->prepareMultiStepForm($form, $form_state, $options);
        }
      }

      // Add multistep form.
      $form['multistep'] = array(
        '#type' => 'portables_multistep',
        '#legend' => $this->getStatusLegend(),
        '#tabs' => $tabs,
        '#active_tab' => $activeStep,
        '#parents' => array('multistep'),
      );
      // Add active form.
      if (isset($allSteps[$activeStep])) {
        $form_state['active_step'] = $allSteps[$activeStep];
        $allSteps[$activeStep]->form($form['multistep'], $form_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form_validate($form, &$form_state) {
    parent::form_validate($form, $form_state);
    if (isset($form_state['active_step'])) {
      /** @var $step PortablesStepUI */
      $step = $form_state['active_step'];
      $step->form_validate($form['multistep'], $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form_extract_values($form, &$form_state) {
    parent::form_extract_values($form, $form_state);
    if (isset($form_state['active_step'])) {
      /** @var $step PortablesStepUI */
      $step = $form_state['active_step'];
      $step->form_extract_values($form['multistep'], $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form_submit($form, &$form_state) {
    parent::form_submit($form, $form_state);
    if (isset($form_state['active_step'])) {
      /** @var $step PortablesStepUI */
      $step = $form_state['active_step'];
      $step->form_submit($form['multistep'], $form_state);
    }
  }

  /**
   * Returns the status legend.
   */
  protected function getStatusLegend() {
    return array(
      self::COMPLETE => t('Complete'),
      self::INCOMPLETE => t('Incomplete'),
    );
  }

  /**
   * Returns a list of step UI objects in order.
   *
   * Implementation should override this method to provide step UI.
   *
   * @return array
   *   An array of step UI info, keyed by the name of each step. The info for a
   *   step contains:
   *   - label: The translated label of the step, to be displayed in the tab
   *     linking to the step form.
   */
  protected function getStepInfo() {
    return array(
      'basic_settings' => array('label' => t('Basic settings')),
    );
  }

  /**
   * Returns the default step.
   *
   * If NULL is returned, the first step will be used as the default step.
   */
  protected function getDefaultStep() {
    // Find the first non-complete step.
    foreach ($this->getStepUIAll() as $name => $ui) {
      if (!$ui || $ui->getStatus() != self::COMPLETE) {
        return $name;
      }
    }
    return NULL;
  }

  /**
   * Returns a lazily created step UI.
   *
   * @param $name
   *   Step name.
   * @return PortablesStepUI
   *   UI for the named step.
   */
  protected function getStepUI($name) {
    if (!isset($this->steps[$name])) {
      $this->steps[$name] = $this->createStepUI($name);
    }
    return $this->steps[$name];
  }

  /**
   * Returns an array of step UIs for all steps.
   *
   * @return PortablesStepUI[]
   */
  protected function getStepUIAll() {
    $steps = array();
    foreach (array_keys($this->getStepInfo()) as $name) {
      $steps[$name] = $this->getStepUI($name);
    }
    return $steps;
  }

  /**
   * Constructs a step UI.
   *
   * @param $name
   *   Step name.
   * @return PortablesStepUI
   *   UI for the named step.
   */
  protected function createStepUI($name) {
    switch ($name) {
      case 'basic_settings':
        return new PortablesBasicSettingsStepUI($this->element);
    }

    // Fail.
    return NULL;
  }
}

/**
 * Base UI for a single step in a multi-step UI.
 *
 * Though the class extends RulesPluginUI, this is not specifically designed as
 * a Faces extender.
 */
abstract class PortablesStepUI extends RulesPluginUI {
  /**
   * Prepares the multi-step UI form.
   */
  public function prepareMultiStepForm(&$form, &$form_state, $options = array()) {}

  /**
   * Returns the status of this step.
   *
   * TODO: Make statuses extensible.
   *
   * @return string
   *   Status flag, one of:
   *   - PortablesMultiStepPluginUI::INCOMPLETE
   *   - PortablesMultiStepPluginUI::COMPLETE
   */
  abstract public function getStatus();

  /**
   * Validates submission of the step configuration form.
   *
   * Form values should not be extracted into the element in this method. Use
   * self::form_extract_values() instead to extract values at the right point.
   */
  public function form_validate($form, &$form_state) {}

  /**
   * Extracts step configuration values into the element.
   */
  public function form_extract_values($form, &$form_state) {}

  /**
   * Processes submission of the step configuration form.
   *
   * Note that the configuration is saved by the main multi-step form.
   */
  public function form_submit($form, &$form_state) {}
}

/**
 * Step UI for basic entity settings.
 */
class PortablesBasicSettingsStepUI extends PortablesStepUI {
  /**
   * {@inheritdoc}
   */
  public function prepareMultiStepForm(&$form, &$form_state, $options = array()) {
    // Hide settings form.
    if (isset($form['settings'])) {
      unset($form['settings']);
    }
  }

  /**
   * Adds the basic settings form as a step.
   */
  public function form(&$form, &$form_state, $options = array()) {
    $this->settingsForm($form, $form_state);
    $form['settings']['#type'] = 'container';
  }

  /**
   * Builds a customized settings form.
   */
  public function settingsForm(&$form, &$form_state) {
    parent::settingsForm($form, $form_state);
    if (!empty($form['settings']['name']['#machine_name']['source'])) {
      $source_parents = array_merge($form['#parents'], $form['settings']['name']['#machine_name']['source']);
      $form['settings']['name']['#machine_name']['source'] = $source_parents;
    }
  }

  /**
   * Extracts element basic settings.
   */
  public function form_extract_values($form, &$form_state) {
    $this->settingsFormExtractValues($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    if (!empty($this->element->name) && !empty($this->element->label)) {
      return PortablesMultiStepPluginUI::COMPLETE;
    }
    return PortablesMultiStepPluginUI::INCOMPLETE;
  }
}

/**
 * UI for processing tasks.
 */
class PortablesTaskUI extends RulesPluginUI {
  // TODO
}
