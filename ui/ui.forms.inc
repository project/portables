<?php
/**
 * @file
 * UI form implementations.
 */

/**
 * Returns the form to add a portable.
 */
function portables_ui_add_portable($form, &$form_state, $base_path) {
  PortablesPluginUI::formDefaults($form, $form_state, $base_path);

  // Initialize portable.
  if (!isset($form_state['rules_config'])) {
    $form_state['rules_config'] = rules_plugin_factory('portable');
    $form_state['rules_config']->module = 'portables';
  }

  // Add plugin form.
  $form_state['rules_config']->form($form, $form_state, array('show settings' => TRUE, 'button' => TRUE, 'init' => TRUE));
  $form['settings']['#collapsible'] = FALSE;
  $form['settings']['#type'] = 'container';
  $form['settings']['label']['#default_value'] = '';
  $form['settings']['#weight'] = -1;
  $form['sources']['#access'] = FALSE;
  $form['tasks']['#access'] = FALSE;

  return $form;
}

/**
 * Validates portable creation form.
 */
function portables_ui_add_portable_validate($form, &$form_state) {
  $form_state['rules_config']->form_validate($form, $form_state);
}

/**
 * Handles portable creation.
 */
function portables_ui_add_portable_submit($form, &$form_state) {
  $rules_config = $form_state['rules_config'];
  $rules_config->form_submit($form, $form_state);
  drupal_set_message(t('Your changes have been saved.'));
}

/**
 * Returns the form to add a source for a portable.
 */
function portables_ui_add_source($form, &$form_state, Portable $portable, $base_path) {
  PortablesPluginUI::formDefaults($form, $form_state, $base_path);

  // Show plugin chooser.
  $form['plugin_name'] = array(
    '#type' => 'portables_plugin_chooser',
    '#title' => t('Choose the type of source to create'),
    '#plugins' => portables_source_plugins(),
    '#default_value' => isset($form_state['values']['plugin_name']) ? $form_state['values']['plugin_name'] : '',
    '#required' => TRUE,
    '#weight' => -2,
  );

  // Show default submit button..
  if (!isset($form_state['rules_config'])) {
    $form['continue'] = array(
      '#type' => 'submit',
      '#name' => 'continue',
      '#submit' => array('portables_ui_create_plugin_submit'),
      '#create_plugin_arguments' => array($portable->name),
      '#value' => t('Continue'),
    );
  }
  // Show plugin creation form.
  else {
    $form['plugin_name']['#disabled'] = TRUE;
    $form_state['rules_config']->form($form, $form_state, array('show settings' => TRUE, 'button' => TRUE, 'init' => TRUE));
    $form['settings']['#collapsible'] = FALSE;
    $form['settings']['#type'] = 'container';
    $form['settings']['label']['#default_value'] = '';
    $form['settings']['#weight'] = -1;
    $form['settings']['vars']['#access'] = FALSE;
  }

  return $form;
}

/**
 * Validates source creation form.
 */
function portables_ui_add_source_validate($form, &$form_state) {
  if (isset($form_state['rules_config'])) {
    $form_state['rules_config']->form_validate($form, $form_state);
  }
}

/**
 * Handles source creation.
 */
function portables_ui_add_source_submit($form, &$form_state) {
  $rules_config = $form_state['rules_config'];
  $rules_config->form_submit($form, $form_state);
  drupal_set_message(t('Your changes have been saved.'));
  $form_state['redirect'] = RulesPluginUI::path($rules_config->name);
}

/**
 * Creates a portable-contained plugin based on the submitted 'plugin_name'.
 */
function portables_ui_create_plugin_submit($form, &$form_state) {
  if (!isset($form_state['rules_config'])) {
    $form_state['triggering_element'] += array('#create_plugin_arguments' => array());
    $factory_arguments = $form_state['triggering_element']['#create_plugin_arguments'];
    array_unshift($factory_arguments, $form_state['values']['plugin_name']);
    $form_state['rules_config'] = call_user_func_array('rules_plugin_factory', $factory_arguments);
    $form_state['rules_config']->module = 'portables';
  }
  $form_state['rebuild'] = TRUE;
}
