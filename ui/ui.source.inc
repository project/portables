<?php
/**
 * @file
 * UI implementations for default source plugins.
 */

/**
 * XPath source UI.
 */
class PortablesXpathSourceUI extends PortablesMultiStepPluginUI {
  // TODO Implement steps: basic settings, XML file, XPath mappings.
  public function form(&$form, &$form_state, $options = array()) {
    parent::form($form, $form_state, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function getStepInfo() {
    return parent::getStepInfo() + array(
      'file' => array('label' => t('XML file')),
      'xpath_mappings' => array('label' => t('XPath mappings')),
    );
  }
}
